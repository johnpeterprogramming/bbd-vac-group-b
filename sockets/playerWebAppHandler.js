// This script handles incoming socket events from the player web app client

// Commands that you can send to the player client
const sendCommands = {
    "questionChanged": (newQuestion) => {
        return "[" + newQuestion + "]";
    }
};

// Events that you can receive from the player client
const receiveEvents = {
    "userLogin": ({ username, password }) => {
        if (username.length > 0 && password.length > 0) {
            return { username, password };
        }
        return null; //returning null does not send anything to the server
    },
    "userAnswered": (answerChosen) => {
        // The return value here is what will be sent to the server
        // These functions are where you can do any necessary preprocessing
        // console.log("USER ANSWERED", answerChosen);
        return answerChosen;
    },
    "userJoined": (data) => {
        // console.log("USER JOINED", data);
        return data;
    }
}

module.exports = {
    receiveEvents,
    sendCommands
};