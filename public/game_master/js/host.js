const socket = io();

const canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');

// --- global variables ---

const cones = [];
const spheres = [];

// --- class definitions ---

// cones will be represented as circles for now...
class Cone {
	constructor(x, y, radius, colour) {
		this.x = x;
		this.y = y;
		this.radius = radius; // will be updated later when images are used for the cones
		this.colour = colour;
		this.state = 'upright'; // not the final data type for the state of the cone
		this.alpha = 1; // opacity of the cone
	}

	draw() {
		ctx.save();
		ctx.globalAlpha = this.alpha;
		ctx.beginPath();
		ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
		ctx.fillStyle = this.colour;
		ctx.fill();
		ctx.restore();
	}

	updateState(state) {
		this.state = state;
	}
}

class Sphere {
	constructor(x, y, radius, colour) {
		this.x = x;
		this.y = y;
		this.radius = radius;
		this.colour = colour;
		this.alpha = 1; // opacity of the cone
	}

	draw() {
		ctx.save();
		ctx.globalAlpha = this.alpha;
		ctx.beginPath();
		ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
		ctx.fillStyle = this.colour;
		ctx.fill();
		ctx.restore();
	}

	updatePosition(x, y) {
		this.x = x;
		this.y = y;
	}
}

// --- functions ---

function animate() {
	requestAnimationFrame(animate);
	ctx.clearRect(0, 0, canvas.width, canvas.height);

	cones.forEach((cone) => {
		cone.draw();
	});

	spheres.forEach((sphere) => {
		sphere.draw();
	});
}

function resizeCanvas() {
	const digitalMap = document.getElementById('digitalMap');
	canvas.width = digitalMap.offsetWidth;
	canvas.height = digitalMap.offsetHeight;
}

// --- main program logic ---

resizeCanvas();
animate();
