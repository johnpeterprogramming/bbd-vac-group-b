const socket = io("http://localhost:3000");

//==================================================
// SEE public/player_web_app/js/playerSockets.js
// The format is basically the same for the below dicts
//==================================================

// Since we're not using typescript, please specify the data types
// you are sending in the params for each of these functions :)
// You can use pseudo-typescript syntax I guess
const sendCommands = {};

// These are events that you want to listen for on the host laptop browser
// Don't change this at runtime, all events should be defined at the start
const receiveEvents = {};


//===== SOCKET CONFIG STUFF =====//
// You can mostly ignore this

var socketId = null;

socket.on('connect', function() {
    console.log('Connected to the server');
    socket.emit("ehlo", "host"); // This tells the server that this is a host
});

socket.on('disconnect', function() {
    console.log('Disconnected from the server');
});

// Auto close the socket connection when the window is closed
window.onbeforeunload = function() {
    socket.disconnect();
    console.log('Socket disconnected due to page unload');
};

// This is the function that you call to dispatch one of the above sendCommands
function dispatch(commandName, data) {
    console.log("Sending event:", commandName, data);
    if (sendCommands[commandName]) {
        const payload = { socketId, data: sendCommands[commandName](data) };
        if (payload !== null) {
            socket.emit("host/" + commandName, payload);
        }
    }
}

// Start listening for receiveEvents
for (const eventName in receiveEvents) {
    if (!receiveEvents.hasOwnProperty(eventName)) continue;

    socket.on(eventName, (data) => {
        receiveEvents[eventName](data);
    });
}